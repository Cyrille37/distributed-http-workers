<?php
namespace App\Http\Controllers\Dhw ;

use App\Http\Controllers\Controller ;
use App\Trades\Agent;
use App\Trades\DhwException;
use App\Trades\Exceptions\AgentException;
use App\Trades\WorkLoadBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AgentController extends Controller
{
    public function run( Request $request )
    {
        /*Log::debug( __METHOD__, [
            'r.query'=>$request->query(),
            'r.contentType'=>$request->getContentType(),
            'r.content'=>$request->getContent() ]);*/
        //Log::debug(__METHOD__,['ajax'=>$request->ajax(), 'wantsJson'=>$request->wantsJson(), 'headers'=>$request->header()]);

        try
        {
            switch( $request->getContentType() )
            {
                case 'json':
                    break;
                default:
                    throw new \InvalidArgumentException('Unknow contentType "'.$request->getContentType().'"');
            }
            $max_runners = $request->max_runners ?: 1 ;

            // Rewrite url: remove "agent" part.
            $base_uri = $request->fullUrl();
            $base_uri = preg_replace( '/\/(agent.*)$/', '', $base_uri);
            //Log::debug( __METHOD__, ['base_uri'=>$base_uri] );

            $wl = WorkLoadBase::build( $request->getContent() );

            //Log::debug( __METHOD__, ['wl'=>$wl] );

            $agentResult = null ;
            $agent = new Agent( $base_uri, $max_runners );
            $agentResult = $agent->run( $wl );    
        }
        catch( \Exception $ex )
        {
            // @see \App\Exceptions\Handler::render() which will format exception as json
            Log::warning(__METHOD__, ['exception'=>$ex ]);
            throw $ex ;
        }

        return response()->json($agentResult);
    }
}
