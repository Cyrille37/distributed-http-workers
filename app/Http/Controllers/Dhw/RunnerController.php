<?php
namespace App\Http\Controllers\Dhw ;

use App\Http\Controllers\Controller ;
use App\Trades\AgentBase;
use App\Trades\Exceptions\DhwException ;
use App\Trades\RunnerBase;
use App\Trades\WorkLoadBase;
use App\Trades\WorkResultError;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RunnerController extends Controller
{
    public function run( Request $request )
    {
        //Log::debug( __METHOD__, ['r.query'=>$request->query(), 'r.contentType'=>$request->getContentType(), 'r.content'=>$request->getContent() ]);

        switch( $request->getContentType() )
        {
            case 'json':
                break;
            default:
                throw new \InvalidArgumentException('Unknow contentType "'.$request->getContentType().'"');
        }
        $wl = WorkLoadBase::build( $request->getContent() );

        $workResults = null ;
        try
        {
            //Log::debug( __METHOD__, ['wl'=>$wl] );
            $runner = RunnerBase::build( $wl );
            $workResults = $runner->run(); 
        }
        catch( \Exception $ex )
        {
            // @see \App\Exceptions\Handler::render() which will format exception as json
            //abort(400, DhwException::build( $ex ) );
            //return response()->json( DhwException::build( $ex ), 400 );
            Log::warning(__METHOD__, ['exception.class'=>get_class($ex),'exception.message'=>$ex->getMessage()] );

            $workResults = [ new WorkResultError( $wl->getBatchs(), $ex ) ];
        }
        return response()->json( $workResults );
    }

}
