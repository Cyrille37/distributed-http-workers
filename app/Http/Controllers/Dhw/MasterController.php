<?php
namespace App\Http\Controllers\Dhw ;

use App\Http\Controllers\Controller ;
use App\Trades\WorkLoadBase;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class MasterController extends Controller
{
    public function index()
    {
        return view('master.index');
    }

    public function run()
    {
        Log::debug( __METHOD__, [] );

        $agent = 'https://dhw.devhost';

        $work = [
            "runners" => 2,
            "trade" =>  [
                "class" => "Addition",
                "data" => ["a"=>1,"b"=>1]
            ]
        ];

        $wl = WorkLoadBase::build( $work );

        $response = Http
            ::withOptions(['verify' => false])
            ->post( $agent.'/api/agent/run', [
                'workload' => $wl,
            ]);
        Log::debug(__METHOD__,[
            'status'=>$response->status(),
            'body'=>$response->body(),
            ]);
        return 'Ok';
    }
}