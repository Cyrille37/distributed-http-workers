<?php

namespace App\Exceptions;

use App\Trades\DhwException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Override default Laravel behavior for Json response,
     * to give a little bit more information when debug=false,
     * and less when debug=true.
     * 
     * Default Laravel Handler behavior :
     * 
     * .env APP_DEBUG=true :
            {
            "message": "Argh!! That is not a great message",
            "exception": "InvalidArgumentException",
            "file": ".../Distributed-Http-Workers/app/Trades/Agent.php", "line": 69,
            "trace": [ ... big array with file, line & so ...
     * .env APP_DEBUG=false:
            { "message": "Server Error" }
     * 
     * @inheritDoc
     * 
     * @param  \Illuminate\Http\Request $request
     * @param  \Throwable $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Throwable $ex)
    {
        if ($request->ajax() || $request->wantsJson())
        {
            Log::warning(__METHOD__, ['ex.class'=>get_class($ex), 'ex.msg'=>$ex->getMessage()]);
            /*Log::info(__METHOD__, ['ex.class'=>get_class($ex)]);
            if( get_class($ex) != DhwException::class )
            {
                $ex = DhwException::build( $ex );
            }*/
            return response()->json( $ex, 400 );
        }

        return parent::render($request, $ex);
    }

}
