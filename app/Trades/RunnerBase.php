<?php
namespace App\Trades;

abstract class RunnerBase {

    public $workLoad ;

    abstract public function run(): array ;

    public function __construct( WorkLoadBase $workLoad )
    {
        $this->workLoad = $workLoad ;
    }

    /**
     * Undocumented function
     *
     * @param Array $workLoadArray
     * @return AgentBase
     */
    public final static function build( WorkLoadBase $wl )
    {
        $runnerClass = '\\App\\Trades\\'.$wl->getTradeClass().'\\Runner';
        //$wlClass = new \ReflectionClass( $wl );
        //$agentClass = $wlClass->getNamespaceName().'\\Agent';
        $agent = new $runnerClass( $wl );
        return $agent ;
    }
}
