<?php
namespace App\Trades;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class AgentResult
{
    public $agent ;
    public $results ;

    public function __construct( Agent $agent )
    {
        $this->agent = $agent;
        $this->results = [];
    }

    public function addWorkResult( WorkResultBase $workResult )
    {
        $this->results[] = $workResult ;
    }

    public static function build( $stringOrArray )
    {
        if( is_string($stringOrArray) )
            $stringOrArray = Trades::json_decode( $stringOrArray );
        $ar = new self( $stringOrArray['agent']);
        foreach( $stringOrArray['results'] as $result )
        {
            $ar->results[] = $result ;
        }
        return $ar ;
    }
}
