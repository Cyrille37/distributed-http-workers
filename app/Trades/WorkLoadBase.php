<?php
namespace App\Trades;

use Illuminate\Support\Facades\Log;

abstract class WorkLoadBase
{
    /**
     * @var array
     */
    public $trade ;
    /**
     * @var array
     */
    public $batchs ;

    protected function __construct( array $workLoadArray )
    {
        $this->trade = $workLoadArray['trade'];
        $this->batchs = $workLoadArray['batchs'];
    }

    public final static function build( $stringOrArray ): WorkLoadBase
    {
        //Log::debug(__METHOD__, ['$stringOrArray'=>$stringOrArray]);
        if( is_string($stringOrArray) )
            $stringOrArray = Trades::json_decode( $stringOrArray );

        $wlClass = '\\App\\Trades\\'.$stringOrArray['trade']['class'].'\\WorkLoad' ;
        $wl = new $wlClass( $stringOrArray );
        return $wl ;
    }

    public function getTradeClass()
    {
        return $this->trade['class'] ;
    }

    public function getBatchs()
    {
        return $this->batchs ;
    }

    public function getTrade()
    {
        return $this->trade ;
    }
}
