<?php
namespace App\Trades\Addition;

use App\Trades\RunnerBase ;
use Illuminate\Support\Facades\Log;

class Runner extends RunnerBase
{
    /**
     * @throws \InvalidArgumentException
     * @return array
     */
    public function run(): array
    {
        //Log::debug(__METHOD__);
        $results = [];
        foreach( $this->workLoad->getBatchs() as $batch )
        {
            //Log::debug(__METHOD__,['$batch'=>$batch]);
            if( (! is_numeric($batch['a'])) || (! is_numeric($batch['b']))  )
                throw new \InvalidArgumentException('"a" & "b" must be numeric values');

            $start = microtime(true);
            $a = $batch['a'] + $batch['b'];
            $results[] = new WorkResult( $batch, ['sum'=>$a], microtime(true) - $start );
        }
        return $results ;
    }
}
