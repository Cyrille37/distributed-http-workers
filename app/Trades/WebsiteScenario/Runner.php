<?php
namespace App\Trades\WebsiteScenario;

use App\Trades\RunnerBase ;
use App\Trades\WebsiteScenario\Results\Result;
use App\Trades\WebsiteScenario\Results\ResultJobDone;
use App\Trades\WorkloadBase ;
use App\Trades\WebsiteScenario\WorkLoad;
use App\Trades\WebsiteScenario\WorkResult;

class Runner extends RunnerBase {

    public function run(): array
    {
        $results = [];

        /**
         * @var \App\Trades\WebsiteScenario\WorkLoad
         */
        $wl = $this->workLoad;
        $scenario = new Scenario( $wl->getScenario() );

        foreach( $this->workLoad->getBatchs() as $batch )
        {
            //Log::debug(__METHOD__,['$batch'=>$batch]);
            $start = microtime(true);

            foreach( $batch as $k => $v )
                $scenario->variableSet( $k, $v );
            $scnResults = $scenario->run();

            $exploration_finish = false ;
            /**
             * @var \App\Trades\WebsiteScenario\Results\Result $result
             */
            foreach( $scnResults as $result )
            {
                if( get_class($result) == ResultJobDone::class )
                {
                    /**
                     * @var \App\Trades\WebsiteScenario\Results\ResultJobDone $result
                     */
                    if( $result->isDone() )
                        $exploration_finish = true ;
                }
            }

            $wr = new WorkResult( $batch, $scnResults, microtime(true) - $start );
            if( $exploration_finish )
                $wr->isFinish = true ;
            $results[] = $wr ;
        }

        return $results;
    }
}
