<?php

namespace App\Trades\WebsiteScenario ;

use App\Trades\Trades;
use App\Trades\WebsiteScenario\Steps\Step;
use App\Trades\Variables;
use App\Trades\VariablesTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Rfc4122\VariantTrait;

class Scenario implements Variables
{
    use VariablesTrait ;

    /**
     * Undocumented variable
     *
     * @var Step[]
     */
    protected $steps = [] ;

    /**
     * @param string|array $data
     */
    public function __construct( $data )
    {
        if( is_string($data) )
        {
            $data = Trades::json_decode( $data );
        }

        $this->variables = [];
        foreach( $data['variables'] as $k=>$v )
        {
            $this->variables[$k] = $v ;
        }

        foreach( $data['steps'] as $stepData )
        {
            $this->steps[] = Step::build( $stepData, $this ) ;
        }
    }

    public function run(): array
    {
        $results = [];

        foreach( $this->steps as $step )
        {
            $start_at = microtime(true);

            $result = $step->run( $this );

            $result->step_start_at = $start_at;
            $result->step_end_at = microtime(true);
            $result->step_ellapsed = $result->step_end_at - $result->step_start_at ;

            $result->variables = $this->variables ;

            $results[] = $result ;
        }

        return $results ;
    }

    public function getStep( $idx )
    {
        if( ! isset($this->steps[ $idx ]) )
            return null ;
        return $this->steps[ $idx ];
    }

}
