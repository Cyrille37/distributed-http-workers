<?php

namespace App\Trades\WebsiteScenario ;

class ScenarioStepsIterator implements \Iterator
{
    protected $scenario ;
    protected $position ;

    public function __construct( Scenario $scenario )
    {
        $this->scenario = $scenario ;
        $this->position = 0;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->scenario->getStep( $this->position );
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++ $this->position;
    }

    public function valid()
    {
        return $this->scenario->getStep( $this->position ) != null
            ? true : false ;
    }

}