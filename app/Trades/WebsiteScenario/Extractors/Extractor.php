<?php

namespace App\Trades\WebsiteScenario\Extractors ;

use App\Trades\Trades;
use App\Trades\Variables;

abstract class Extractor
{
    public abstract function extract( &$content );

    /**
     * Undocumented variable
     *
     * @var Variables
     */
    public $variables ;

    /**
     * @param string|array $data
     * @param Variables $variables
     */
    public function __construct( &$data, Variables $variables )
    {
        $this->variables = $variables ;
    }

    /**
     * @param string|array $data
     * @return Step
     */
    public static function build( &$data, Variables $variables )
    {
        if( is_string($data) )
        {
            $data = Trades::json_decode( $data );
        }

        $class = '\\'.__NAMESPACE__.'\\'. $data['class'];
        $xx = new $class( $data, $variables );
        return $xx ;
    }

    protected function setVariable( $name, $value ): bool
    {
        if( $this->variables && $this->variable )
        {
            $this->variables->variableSet($name, $value);
            return true ;
        }
        return false ;
    }
}