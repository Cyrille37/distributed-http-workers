<?php

namespace App\Trades\WebsiteScenario\Extractors ;

use App\Trades\Variables;
use DOMDocument;
use DOMXPath;

class XPathExtractor extends Extractor
{
    /**
     * DOMXpath query.
     * @see https://www.php.net/manual/en/class.domxpath.php
     * @var string
     */
    protected $query ;

    /**
     * @inheritDoc
     * @return XPathExtractor
     */
    public function __construct( &$data, Variables $variables )
    {
        parent::__construct( $data, $variables );
        $this->query = $data['query'] ;
    }

    /**
     * Transforms $content as a DOMDocument and query it with the $this->query DOMXPath.
     *
     * @see https://www.php.net/manual/en/class.domdocument.php
     * 
     * @param string $content
     * @return void
     */
    public function extract( &$content )
    {
        $query = $this->variables->variableProcess( $this->query );
        $dom = new DOMDocument();

        $html = $content ;
        // Need Tidy lib
        //$html = \tidy_repair_string( $html );
        $html = \mb_convert_encoding( $html,'HTML-ENTITIES','UTF-8' );

        $internalErrors = libxml_use_internal_errors(true);
        $dom->recover = true;
        $dom->strictErrorChecking = false;
        $dom->loadHTML(
            $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );
        //echo 'LibXml errors: ', var_export(libxml_get_errors(),true ), "\n";
        //echo 'LibXml errors count: ', count(libxml_get_errors()), "\n";
        libxml_clear_errors();

        $xpath = new DOMXPath($dom);

        // '//*[@name="twitter:title"]/@content'
        return $xpath->query( $query );
    }

}
