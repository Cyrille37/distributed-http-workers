<?php

namespace App\Trades\WebsiteScenario\Extractors ;

use App\Trades\Variables;
use DOMAttr;
use DOMNodeList;

class XPathDomAttrExtractor extends XPathExtractor
{
    /**
     * The variable name to set in Variables container.
     *
     * @var string
     */
    protected $variable ;

    /**
     * @inheritDoc
     * @return XPathDomAttrExtractor
     */
    public function __construct( &$data, Variables $variables )
    {
        parent::__construct( $data, $variables );
        $this->variable = $data['variable'] ;
    }

    public function extract( &$content )
    {
        $variable = $this->variables->variableProcess( $this->variable );
        //echo __METHOD__, ' VAR:', $this->variable, ', Q:', $this->query,"\n";
        /**
         * @var DOMNodeList|null $elements
         */
        $elements = parent::extract( $content );
        if( empty($elements) || $elements->count() == 0)
            return null ;

        $element = $elements[0];
        //echo __METHOD__, ' CLASS:', get_class($element),"\n";
        if( ! ($element instanceof DOMAttr) )
            return null ;

        //echo __METHOD__, ' VAR:',$this->variable, ', VALUE:', $element->value,"\n";
        $this->setVariable( $variable, $element->value );
        return $element->value;
    }

}
