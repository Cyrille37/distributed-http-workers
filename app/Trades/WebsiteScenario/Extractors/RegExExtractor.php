<?php

namespace App\Trades\WebsiteScenario\Extractors ;

use App\Trades\Variables;
use Illuminate\Support\Facades\Log;

class RegExExtractor extends Extractor
{
    /**
     * @var string
     */
    protected $query ;
    /**
     * The variable name to set in Variables container.
     *
     * @var string
     */
    protected $variable ;

    /**
     * @inheritDoc
     * @return Extractor
     */
    public function __construct( &$data, Variables $variables )
    {
        parent::__construct( $data, $variables );
        $this->query = $data['query'] ;
        $this->variable = $data['variable'] ;
    }

    public function extract( &$content )
    {
        $query = $this->variables->variableProcess( $this->query );
        $variable = $this->variables->variableProcess( $this->variable );

        /*
        Attention: special regex escaping for "query" & "variables" :
            - the variable content must be escaped for regex's delimiter "/"
            - here variable "home" must be "https:\\/\\/pdpat.devhost\\/"
                "<li id='wp-admin-bar-site-name' class=\"menupop\">.* href='{{home}}'>(.*)<\\/a>",
        */

        // #3 full query in "query' input
        //preg_match( '/'.$query.'/mU', $content, $matches );
        preg_match( $query, $content, $matches );
        //Log::debug(__METHOD__,['$query'=>$query, '$matches'=>$matches ]);

        if( empty($matches) || count($matches)!=2 )
            return null ;
        $this->setVariable($variable, $matches[1] );
        return $matches[1] ;
    }
}
