<?php

namespace App\Trades\WebsiteScenario\Results ;

use Symfony\Component\HttpFoundation\Response ;

class ResultSleep extends Result
{
    public $sleep_ms ;

    public function __construct( int $sleep_ms )
    {
        parent::__construct();

        $this->sleep_ms = $sleep_ms ;        
    }

    public function isError(): bool 
    {
        return false ;
    }
}
