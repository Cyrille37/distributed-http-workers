<?php

namespace App\Trades\WebsiteScenario\Results ;

use Symfony\Component\HttpFoundation\Response ;

class ResultJobDone extends Result
{
    public $done ;

    public function __construct( bool $done  )
    {
        parent::__construct();

        $this->done = $done ;        
    }

    public function isDone(): bool 
    {
        return $this->done ;
    }

    public function isError(): bool 
    {
        return false ;
    }
}
