<?php

namespace App\Trades\WebsiteScenario\Results ;

use Symfony\Component\HttpFoundation\Response ;

class ResultHttp extends Result
{
    public $http_uri ;
    public $http_method ;
    public $req_duration ;
    public $resp_status ;
    public $resp_body_size ;

    public function __construct()
    {
        parent::__construct();
    }

    public function isError(): bool
    {
        if( $this->resp_status >= Response::HTTP_OK
            && $this->resp_status < Response::HTTP_BAD_REQUEST  )
        {
            return false ;    
        }
        return true ;
    }

}