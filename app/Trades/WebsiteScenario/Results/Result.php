<?php

namespace App\Trades\WebsiteScenario\Results ;

abstract class Result
{
    public $step_start_at ;
    public $step_end_at ;
    public $step_ellapsed ;

    public $variables ;

    public abstract function isError(): bool ;

    public function __construct()
    {
        $this->class = static::class ;        
    }
}