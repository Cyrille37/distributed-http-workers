<?php
namespace App\Trades\WebsiteScenario\Steps ;

use App\Trades\WebsiteScenario\Results\Result;
use App\Trades\WebsiteScenario\Results\ResultJobDone;
use App\Trades\WebsiteScenario\Results\ResultSleep;
use App\Trades\Variables;
use Illuminate\Support\Facades\Log;

class JobDone extends Step
{
    public $variable ;
    public $regex ;
    public $inverse ;

    protected function __construct( $data, Variables $variables = null )
    {
        parent::__construct( $data, $variables );
        $this->variable = $data['variable'] ;
        $this->regex = $data['regex'] ;
        $this->inverse = $data['inverse'];
    }

    public function run(): Result
    {
        $value = $this->variables->variableGet( $this->variableProcess( $this->variable ) );
        $regex = $this->variableProcess( $this->regex );
        //Log::debug(__METHOD__, ['$this->regex'=>$this->regex,'regex'=>$regex,'value'=>$value]);
        if( $this->inverse )
            return new ResultJobDone( ! preg_match( $regex, $value ) );
        else
            return new ResultJobDone( preg_match( $regex, $value ) );
    }

}
