<?php

namespace App\Trades\WebsiteScenario\Steps ;

use Illuminate\Support\Facades\Http as FacadesHttp;
use App\Trades\WebsiteScenario\Extractors\Extractor ;
use App\Trades\WebsiteScenario\Results\Result;
use App\Trades\WebsiteScenario\Results\ResultHttp;
use App\Trades\Variables;
use Carbon\Carbon;
use GuzzleHttp\Cookie\CookieJar;

abstract class Http extends Step
{
    /**
     * @var string
     */
    protected $url ;
    /**
     * @var array
     */
    protected $params ;
    /**
     * @var Extractor[]
     */
    protected $extractors ;

    /**
     * Permits to store cookies between requests.
     * (ttl) One used for a Scenario.
     * @var CookieJar $cookies
     */
    static $cookies ;

    protected function __construct( $data, Variables $variables = null )
    {
        parent::__construct( $data, $variables );

        if( self::$cookies == null )
        {
            self::$cookies = new CookieJar();
        }

        $this->url = $data['url'];

        $this->params = [];
        if( isset($data['params']) )
        {
            $this->params = $data['params'];
        }

        $this->extractors = [];
        if( isset($data['extractors']) )
        {
            foreach( $data['extractors'] as $d )
            {
                $this->extractors[] = Extractor::build( $d, $variables );
            }    
        }
    }

    /**
     * @see https://laravel.com/docs/8.x/http-client
     */
    public function run( /*Variables $variables = null*/ ): Result
    {
        $result = new ResultHttp();

        $url = $this->variableProcess( $this->url );

        $params = [];
        foreach( $this->params as $k => $v )
        {
            $params[ $this->variableProcess( $k ) ] = $this->variableProcess( $v );
        }
        //echo __METHOD__.' url:'.$url, ', class:', static::class,', params:', var_export($params),"\n";

        $http = FacadesHttp
            ::withOptions([
                'verify' => false,
                'allow_redirects' => true,
                'cookies' => self::$cookies,
            ]);

        $req_start_at = microtime(true);
        switch( static::class )
        {
            case Get::class:
                $result->http_method = 'GET';
                $response = $http->get( $url, $params );
                break;

            case Post::class:
                $result->http_method = 'POST';
                // For "application/x-www-form-urlencoded", raw, "multi-part, "json" ...
                // @see https://laravel.com/docs/8.x/http-client
                /*$response = $http->withHeaders([
                    //'Referer' => 'https://pdpat.devhost/wp-login.php',
                ])->asForm()->post( $url, $params);*/

                $response = $http->asForm()->post( $url, $params);

                //echo var_export($response),"\n",$response->body(),"\n";
                break;
            default:
                throw new \InvalidArgumentException('Unknow how processing "'.static::class.'"');
        }
        $result->http_uri = $response->effectiveUri()->__toString();

        //echo var_export($response),"\n";
        $result->req_duration = microtime(true) - $req_start_at ;
        $result->resp_status = $response->status() ;
        $content = $response->body();
        $result->resp_body_size = strlen($content);

        foreach( $this->extractors as $xx )
        {
            $xx->extract( $content );
        }

        return $result ;
    }
}
