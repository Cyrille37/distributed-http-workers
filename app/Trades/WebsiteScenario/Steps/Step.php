<?php

namespace App\Trades\WebsiteScenario\Steps ;

use App\Trades\Trades;
use App\Trades\WebsiteScenario\Results\Result;
use App\Trades\Variables;
use Illuminate\Support\Facades\Log;

abstract class Step
{
    /**
     * @var Variables
     */
    protected $variables ;

    public abstract function run( /*Variables $variables = null*/ ): Result ;

    public function variableProcess( $var )
    {
        if( $this->variables )
            return $this->variables->variableProcess( $var );
        return $var ;
    }

    protected function __construct( $data, Variables $variables = null )
    {
        $this->variables = $variables ;
    }

    /**
     * @param string|array $data
     * @return Step
     */
    public static function build( $data, Variables $variables = null )
    {
        if( is_string($data) )
        {
            $data = Trades::json_decode( $data );
        }

        $class = '\\'.__NAMESPACE__.'\\'. $data['class'];
        $s = new $class( $data, $variables );
        return $s ;
    }
}