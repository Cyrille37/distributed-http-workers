<?php
namespace App\Trades\WebsiteScenario\Steps ;

use App\Trades\WebsiteScenario\Results\Result;
use App\Trades\WebsiteScenario\Results\ResultSleep;
use App\Trades\Variables;
use Illuminate\Support\Facades\Log;

class SleepStep extends Step
{
    public $milliseconds ;

    protected function __construct( $data, Variables $variables = null )
    {
        parent::__construct( $data, $variables );
        $this->milliseconds = $data['ms'] ;
    }

    public function run(): Result
    {
        if( is_array( $this->milliseconds ) )
        {
            $min = intval( $this->variableProcess($this->milliseconds[0]) );
            $max = intval( $this->variableProcess($this->milliseconds[1]) );
            $ms = rand( $min, $max );
        }
        else
        {
            $ms = intval( $this->variableProcess($this->milliseconds) );
        }

        usleep( 1000 * $ms );
        return new ResultSleep( $ms );
    }

}
