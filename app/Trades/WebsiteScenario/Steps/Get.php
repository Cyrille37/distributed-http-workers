<?php

namespace App\Trades\WebsiteScenario\Steps ;

use App\Trades\Variables;

class Get extends Http
{
    protected function __construct( $data, Variables $variables = null )
    {
        parent::__construct( $data, $variables );
    }
}
