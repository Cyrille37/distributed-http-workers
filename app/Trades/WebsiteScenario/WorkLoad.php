<?php

namespace App\Trades\WebsiteScenario ;

use App\Trades\WorkLoadBase;

class WorkLoad extends WorkLoadBase
{
    public function __construct( Array $workLoadArray )
    {
        parent::__construct( $workLoadArray );
    }

    /**
     * @return Scenario
     */
    public function getScenario()
    {
        return $this->trade['data'] ;
    }
}