<?php
namespace App\Trades;

use Illuminate\Support\Facades\Log;

abstract class WorkResultBase
{
    const INVALID_ELLAPSED = -1 ;
    public $class ;
    public $batch ;
    public $result ;
    public $ellapsed_ms ;
    public $isFinish = false ;

    public function __construct( array $batch, array $result, float $ellapsed_ms )
    {
        //$this->class = get_class($this) ;
        $this->class = static::class ;
        $this->batch = $batch;
        $this->result = $result;
        $this->ellapsed_ms = $ellapsed_ms ;
    }

    public static function build( $stringOrArray, $tradeClass = null )
    {
        //Log::debug(__METHOD__, ['$tradeClass'=>$tradeClass,'$stringOrArray'=>$stringOrArray]);

        if( is_string($stringOrArray) )
            $stringOrArray = Trades::json_decode( $stringOrArray );

        if( isset($stringOrArray['class']) )
            $wrClass = $stringOrArray['class'] ;
        else
            $wrClass = '\\App\\Trades\\'.$tradeClass.'\\WorkResult' ;
        switch( $wrClass )
        {
            case WorkResultError::class:
                $exception = new $stringOrArray['result']['exception']($stringOrArray['result']['message']);
                $wr = new WorkResultError( $stringOrArray['batch']??[], $exception );
                $wr->result['origin'] = $stringOrArray['result']['origin'];
                $wr->result['ellapsed_ms'] = $stringOrArray['ellapsed_ms'];
                break;
            default:
                $wr = new $wrClass( $stringOrArray['batch']??[], $stringOrArray['result']??[], $stringOrArray['ellapsed_ms']??0);
                $wr->class = $wrClass ;
                $wr->isFinish = $stringOrArray['isFinish'];
        }

        return $wr ;
    }

    public function getClass()
    {
        return $this->class ;
    }

    /**
     * @return array
     */
    public function getBatch() : array
    {
        return $this->batch ;
    }

    /**
     * @return array
     */
    public function getResult() : array
    {
        return $this->result ;
    }

    /**
     * @return float
     */
    public function getEllapsedMs() : float
    {
        return $this->ellapsed_ms ;
    }

    public function isFinish() : bool
    {
        return $this->isFinish;
    }
}
