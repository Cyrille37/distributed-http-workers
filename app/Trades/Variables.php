<?php

namespace App\Trades ;

interface Variables
{
    public function variableProcess( $str );
    public function variableSet( $name, $value );
    public function variableGet( $name, $default = null );
}