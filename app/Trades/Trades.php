<?php
namespace App\Trades;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;

class Trades
{
    const LOOP_MODE_BATCHS = 'batchs' ;
    const LOOP_MODE_COUNT = 'count' ;

    const AGENT_DEF_SEP = '|' ;
    const AGENT_DEF_DEFAULT_RUNNERS = 1 ;

    const GUZZLE_ERROR = 'rejected';
    const GUZZLE_OK = 'fulfilled';

    const JSON_DECODE_DEPTH = 512 ;

    public static function json_decode( &$string )
    {
        return json_decode(
            $string,
            $associative=true,
            Trades::JSON_DECODE_DEPTH, JSON_OBJECT_AS_ARRAY|JSON_THROW_ON_ERROR
        );
    }

    /**
     * A http client with commont configuration.
     *
     * @return GuzzleHttp\Client
     */
    public static function createHttpClient()
    {
        return new Client( [
            'verify'=>false,
            //'cookies' => true,
            'headers'=>[
                // Important to make true "$request->wantsJson()"
                'Accept' => 'application/json',
                'user-agent' => 'Dhw 1.0'
            ]
        ]);
    }

    public static function whereAmI()
    {
        static $w ;
        if( $w != null )
            return $w ;

        $w = gethostname()
            .'/'.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '' )
            .'/'.(isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : '' )
            .'/'.php_sapi_name()
            ;
        if( App::runningInConsole() )
        {
            $w .= '/'.implode('-', $_SERVER['argv'] );
        }
        else
        {
            $w .= '/'.(isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : 'unknow');
        }
        return $w ;
    }

}