<?php
namespace App\Trades ;

class WorkResultError extends WorkResultBase
{
    public function __construct( $batch, \Throwable $throwable )
    {
        $result = [
            'exception' => get_class($throwable),
            'message' => $throwable->getMessage(),
            'origin' => Trades::whereAmI(),
        ];
        parent::__construct( $batch, $result, self::INVALID_ELLAPSED );
    }

    public function getOrigin()
    {
        return $this->result['origin'];
    }

    public function getMessage()
    {
        return $this->result['message'];
    }
    public function getExceptionClass()
    {
        return $this->result['exception'];
    }
}
