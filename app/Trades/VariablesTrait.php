<?php

namespace App\Trades ;

trait VariablesTrait
{
    protected $variables = [] ;

    public function variableSet( $name, $value )
    {
        //Log::debug(__METHOD__,[ '$name'=>$name,'$value'=>$value ]);
        $this->variables[$name] = $value ;
    }

    public function variableGet( $name, $default = null )
    {
        if( isset($this->variables[$name]) )
            return $this->variables[$name] ;
        return $default ;
    }

    public function getVariables()
    {
        return $this->variables ;
    }

    /**
     * Process "{{variablename}}" or "{{variable.name}}" replacement.
     * @param string $str
     * @return void
     */
    public function variableProcess( $str )
    {
        //echo __METHOD__, ' VAR:',$str,"\n";
        //echo __METHOD__, ' VAR:',$str, ', VARS:', print_r($this->variables,true),"\n";
        $variables =& $this->variables ;
        return preg_replace_callback(
            '#\{\{([a-z0-9\.\-_]+)\}\}#',
            function($m) use ($variables)
            {
                //echo __METHOD__, ' VAR.MATCH:',$m[1], ', VARS:', print_r($variables,true),"\n";
                //echo __METHOD__, ' VAR.MATCH:',$m[1] ,"\n";
                if( isset($variables[$m[1]]) )
                {
                    //echo __METHOD__, ' VAR.REPL:',$m[1],'->',$variables[$m[1]],"\n";
                    return $variables[$m[1]];
                }
                return '{{'.$m[1].'}}';
            },
            $str );
    }

}