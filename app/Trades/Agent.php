<?php
namespace App\Trades;

use App\Trades\Exceptions\DhwException;
use GuzzleHttp\Promise;
use Illuminate\Support\Facades\Log;

class Agent
{
    /**
     * @var string
     */
    public $url ;
    /**
     * @var int
     */
    public $max_runners ;

    public function __construct( $url, $max_runners )
    {
        $this->url = $url;
        $this->max_runners = $max_runners;
    }

    /**
     * Processing all batchs present in the workload,
     * with max_runners parallel/asynchrone http calls.
     *
     * @param WorkLoadBase $workLoad
     * @return AgentResult
     */
    public function run( WorkLoadBase $workLoad ): AgentResult
    {
        $agentResult = new AgentResult( $this );

        $runner_url = $this->url.'/runner' ;

        $http = Trades::createHttpClient();

        $trade = $workLoad->getTrade();
        $batchs = $workLoad->getBatchs();

        $batchsCount = count($batchs);
        $batchsDone = 0 ;
        while( $batchsDone < $batchsCount )
        {
            $promises = [];
            $batchsRest = $batchsCount - $batchsDone ;

            for( $r=0; $r < min($this->max_runners, $batchsRest ); $r++ )
            {
                $batch = & $batchs[ $batchsDone ];
                $wl = WorkLoadBase::build( [
                    'batchs' => [$batch],
                    'trade' => &$trade,
                ] );
                $promises[] = $http->postAsync(
                    $runner_url,
                    [
                        'json'=>$wl,
                    ]
                );
                $batchsDone ++ ;
            }

            // Wait for the requests to complete, even if some of them fail
            $responses = Promise\Utils::settle($promises)->wait();

            // Values returned above are wrapped in an array with 2 keys:
            // - "state" either "fulfilled" or "rejected"
            // - and "value" contains the response
            //
            $respCount = 0 ;
            foreach( $responses as $response )
            {
                //echo 'Agent got a Runner[',($respCount),'] response state: ', $response['state'],"\n";
                $wr = null ;
                switch( $response['state'] )
                {
                    case Trades::GUZZLE_OK:
                        /**
                         * @var \GuzzleHttp\Psr7\Response $response
                         */
                        $response = $response['value'];
                        $content = $response->getBody()->getContents();
                        //Log::debug( __METHOD__, ['result'=>$content]);

                        foreach( Trades::json_decode( $content ) as $result )
                        {
                            $wr = WorkResultBase::build( $result );
                            $agentResult->addWorkResult( $wr );
                        }
                        break;

                    case Trades::GUZZLE_ERROR:
                        /**
                         * \GuzzleHttp\Exception\ClientException $ex ;
                         * @var \GuzzleHttp\Exception\ServerException $ex ;
                         */
                        $ex = $response['reason'] ;
                        Log::warning(__METHOD__, ['reason'=>$response['reason'] ]);
                        /*
                        //$err = 'Ex Message: '. $ex->getMessage()."\n" ;
                        if( $response['reason']->hasResponse() && $ex->getResponse()->getStatusCode()==400 )
                        {
                            $content = $ex->getResponse()->getBody()->getContents() ;
                            //$err.= 'Ex Response Status: '.$ex->getResponse()->getStatusCode()."\n";
                            //$err.='Ex Response Content: '.$content."\n";
                            //echo 'Agent got error : ', $err,"\n";
                        }
                        else
                        {
                            throw $ex ;
                        }
                        */
                        throw $ex ;

                        break;

                    default:
                        throw new \RuntimeException('Unknow Guzzle response state "'.$response['state'].'"');
                }
                $respCount ++ ;
            }

        }

        return $agentResult ;
    }

    public function getUrl()
    {
        return $this->url ;
    }
    public function getMaxRunners()
    {
        return $this->max_runners ;
    }

    public static function build( $stringOrArray )
    {
        if( is_string($stringOrArray) )
            $stringOrArray = Trades::json_decode( $stringOrArray );
        return new Agent( $stringOrArray['url'], $stringOrArray['max_runners'] );
    }
}
