<?php

namespace App\Console\Commands;

use App\Trades\WebsiteScenario\Scenario;
use Carbon\Carbon;
use Illuminate\Console\Command;
use InvalidArgumentException;

class WebScenarioCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dhw:webScenario'
        .' {file : Scenario filename}'
        .' {--var=* : Zero or more variable(s) to set like "name=value"}'
        ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run a WebsiteScenario'
        ;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = $this->argument('file') ;
        $scn = file_get_contents($file);

        $start_at = microtime(true);

        $scn = new Scenario( $scn );

        // Command has variables

        $variables = $this->option('var');
        foreach( $variables as $var )
        {
            $var = explode('=',$var);
            if( count($var) != 2 )
                throw new \InvalidArgumentException('"var" option must be a key/value pair like "name=value"');
            $scn->variableSet( $var[0], $var[1] );
        }
        // echo 'Variables: ',print_r($scn->getVariables()),"\n";

        // Scenario run 

        $results = $scn->run();

        $this->info( 'Scenario results: '.var_export($results,true) );

        echo 'Scenario ellapsed seconds: ', number_format( microtime(true) - $start_at, 3) ,"\n";
    }
}