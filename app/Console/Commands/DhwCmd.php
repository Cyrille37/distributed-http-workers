<?php
namespace App\Console\Commands;

use App\Trades\Trades;
use App\Trades\Variables;
use App\Trades\VariablesTrait;
use App\Trades\WorkLoadBase;
use App\Trades\WorkResultBase;
use Carbon\Carbon;
use Illuminate\Console\Command;
use InvalidArgumentException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Support\Facades\Log;

/**
 * examples:
 * - Agent url
 *  - https://dhw.devhost/api/dhw/agent
 * 
 * Les batchs du fichier distribués sur 1 agent avec 1 runner
 * ./artisan dhw:dhw ./doc/example_data/AgentWorkload-Addition_ok.json batchs --agent="https://dhw.devhost/api/dhw/agent|1"
 * 
 * Les batchs du fichier distribués sur 1 agent avec 3 runners
 * ./artisan dhw:dhw ./doc/example_data/AgentWorkload-Addition_ok.json batchs --agent="https://dhw.devhost/api/dhw/agent|3"
 * 
 * Les batchs du fichier distribués sur 1 agent avec 2 runners, les batchs sont rejoués pour arriver à 5 exécutions.
 * ./artisan dhw:dhw ./doc/example_data/AgentWorkload-Addition_ok.json count --count=5 --agent="https://dhw.devhost/api/dhw/agent|2"
 * 
 * Et pour voir un résult en erreur:
 * ./artisan dhw:dhw ./doc/example_data/AgentWorkload-Addition_error.json batchs --agent="https://dhw.devhost/api/dhw/agent|1"
 * 
 */
class DhwCmd extends Command implements Variables
{
    use VariablesTrait ;

    /**
     * @var string
     */
    protected $signature = 'dhw:dhw'
        .' {file : Worload filename}'
        .' {loop : The loop mode as "'.Trades::LOOP_MODE_BATCHS.'" or "'.Trades::LOOP_MODE_COUNT.'"}'
        .' {--agent=* : Agent(s) in form of "<agent url>'.Trades::AGENT_DEF_SEP.'[max runners]}'
        .' {--count= : Mandatory when "loop=='.Trades::LOOP_MODE_COUNT.'", the count of total loops}'
        .' {--input_csv= : Filename to read batchs (batchs in workload won’t be used)}'
        .' {--output_csv= : Filename to write Results as CSV}'
        .' {--var=* : Zero or more variable(s) like "name=value" to replace in "batchs"}'
        ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run a Workload'
        ;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = $this->argument('file');
        $loop_mode = $this->argument('loop');
        $agents_def = $this->option('agent');
        $input_csv = $this->option('input_csv');
        $output_csv = $this->option('output_csv');
        $start_at = microtime(true);

        // Read workload

        if( ! is_readable($file) )
            throw new \InvalidArgumentException('Cannot read file "'.$file.'"');

        if( $output_csv && ! is_writable($output_csv) )
            throw new \InvalidArgumentException('Cannot write file "'.$output_csv.'"');

        if( $input_csv && ! is_readable($input_csv) )
            throw new \InvalidArgumentException('Cannot read file "'.$input_csv.'"');

        $masterWL = WorkLoadBase::build( file_get_contents($file) );
    
        // Parse agents definition

        $agents = $this->buildAgentsList( $agents_def );
        //echo 'Agents: ',var_export($agents,true),"\n";
        $agentsCount = count( $agents );

        // extract optional variables

        foreach( $this->option('var') as $var )
        {
            $var = explode('=',$var);
            if( count($var) != 2 )
                throw new \InvalidArgumentException('"var" option must be a key/value pair like "name=value"');
            $this->variableSet( $var[0], $var[1] );
        }

        // Extract batchs from workload or csv

        if( $input_csv )
        {
            $batchs = $this->buildBatchsFromCsv( $input_csv );
        }
        else
        {
            $batchs = $masterWL->getBatchs();
        }

        $batchsCount = count($batchs);

        // Processing batchs

        $loopsTotal = null ;
        switch( $loop_mode )
        {
            case Trades::LOOP_MODE_BATCHS:
                $loopsTotal = $batchsCount ;
                break;
            case Trades::LOOP_MODE_COUNT:
                if( empty($this->option('count')) )
                    throw new \InvalidArgumentException('Loop mode "count" needs "count" option');
                $loopsTotal = $this->option('count');
                break;
            default:
                throw new \InvalidArgumentException('Unknow loop_mode "'.$loop_mode.'"');
        }

        $batchsDone = 0 ;

        $trade = $masterWL->getTrade();

        $output_fp = null ;
        if( $output_csv )
            $output_fp = fopen( $output_csv, 'w');

        $http = Trades::createHttpClient();

        //
        // 1. Fill $promises with http queries.
        //

        $isFinish = false ;

        while( ($batchsDone < $loopsTotal) && ! $isFinish )
        {
            $this->info('batchsDone:'.$batchsDone.' in '.number_format(microtime(true)-$start_at, 6) );

            $promises = [];
            $batchsRest = $loopsTotal - $batchsDone ;
            for( $a=0; $a < min($agentsCount, $batchsRest ); $a++ )
            {
                $agent = $agents[ $a ];
                $max_runners = $agent['max_runners'];
                $agentBatchs = [];

                for( $b=0; $b < min($max_runners, $batchsRest); $b++ )
                {
                    if( $loopsTotal > 1 )
                        $bi = $batchsDone % $batchsCount ;
                    else
                        $bi = $batchsDone ;

                    //Log::debug(__METHOD__, ['a'=>$a,'b'=>$b, 'bi'=>$bi, 'batchs'=>count($agentBatchs), 'batchsCount'=>$batchsCount, 'batchsDone'=>$batchsDone, 'batchsRest'=>$batchsRest,'$max_runners'=>$max_runners]);

                    $batch = & $batchs[ $bi ];
                    foreach( $batch as $k => $v )
                    {
                        $batch[$k] = $this->variableProcess( $v );
                    }
                    $agentBatchs[] = $batch;

                    $batchsDone ++ ;
                }
                $batchsRest = $loopsTotal - $batchsDone ;

                $wl = WorkLoadBase::build( [
                    'batchs' => $agentBatchs,
                    'trade' => & $trade,
                ] );
                $promises[] = $http->postAsync(
                    $agent['url'].'?max_runners='.$max_runners,
                    [ 'json'=>$wl, ]
                );
            }

            //
            // 2. Wait for the requests to complete, even if some of them fail
            //

            $responses = Promise\Utils::settle($promises)->wait();

            // 3. Processing responses
            //
            // $response is wrapped in an array with 2 keys:
            // - "state" either fulfilled or rejected.
            //  - if "state" == "rejected" then the key "reason" contains a \GuzzleHttp\Exception\ClientException
            //  - if "state" == "fulfilled" then the key "value" contains the response
            //
            $respCount = 0 ;
            foreach( $responses as $response )
            {
                //var_dump( $response );
                switch( $response['state'] )
                {
                    case Trades::GUZZLE_OK:
                        /**
                         * @var \GuzzleHttp\Psr7\Response $response
                         */
                        $response = $response['value'];
                        $contents = $response->getBody()->getContents();
                        //Log::debug(__METHOD__,['contents'=>$contents]);
                        $contents = Trades::json_decode($contents);
                        $this->comment('DHW Agent['.$respCount.'] '.print_r($contents['agent'],true));
                        foreach( $contents['results'] as $result )
                        {
                            $this->comment( var_export($result,true) );
                            $wr = WorkResultBase::build( $result );
                            if( $wr->isFinish() )
                                $isFinish = true ;
                        }
                        break;

                    case Trades::GUZZLE_ERROR:
                        /**
                         * \GuzzleHttp\Exception\ClientException $ex ;
                         * @var \GuzzleHttp\Exception\ServerException $ex ;
                         */
                        $ex = $response['reason'] ;

                        $err = 'Ex Message: '. $ex->getMessage()."\n" ;
                        if( $response['reason']->hasResponse() )
                        {
                            $err.= 'Ex Response Status: '.$ex->getResponse()->getStatusCode()."\n";
                            //$err.='Ex Response Content: '.$ex->getResponse()->getBody()->getContents()."\n";
                        }
                        echo 'ERROR! Dhw got Agent[',$respCount,'] error: ', $err,"\n";
                        Log::warning(__METHOD__, ['$respCount'=>$respCount, '$err'=>$err ]);
                        break;

                    default:
                        throw new \RuntimeException('Unknow Guzzle response state "'.$response['state'].'"');
                }
                $respCount ++ ;
            }

        }

        if( $output_fp )
        {
            fclose( $output_fp );
            $this->info('Results written as CSV in "'.$output_csv.'"');
        }

        $this->info( 'batchsCount:'.$batchsCount.', batchsDone:'.$batchsDone
            .', isFinish:'.($isFinish?'true':'false')
            .', ellapsed:'.number_format(microtime(true)-$start_at, 6)
        );
    }

    /**
     * Cannot be a generator, because data validity must be known before iteration.
     * @param string[] $agents_def An array of string as "<agent_url>|[maxrunner]".
     * @return array[] An array of array with keys "url" & "max_runners".
     */
    protected function & buildAgentsList( array &$agents_def )
    {
        //echo 'agents_def:', count($agents_def),"\n"; 

        if( count($agents_def)==0 )
            throw new \InvalidArgumentException('Missing agent(s)');

        $agents = [];
        foreach( $agents_def as $agent_def)
        {
            $agent = explode(Trades::AGENT_DEF_SEP, $agent_def);
            if( count($agent)==0 || count($agent)>2 )
                throw new \InvalidArgumentException('Failed to parse agent def "'.$agent_def.'"');
            if( count($agent)==1 )
                $agent[1] = Trades::AGENT_DEF_DEFAULT_RUNNERS ;
            //yield $agent ;
            $agents[] = [
                'url'=>$agent[0],
                'max_runners' => $agent[1],
            ];
        }
        return $agents ;
    }

    /**
     * Il faudrait un générateur ou autre, mais il faut connaitre le nombre total avant de commencer le boulot ...
     */
    protected function & buildBatchsFromCsv( $csv_filename ) : array
    {
        $csv = fopen( $csv_filename, 'r');
        $batchs = [];
        $headers = fgetcsv( $csv );
        while( $row = fgetcsv( $csv ) )
        {
            $batch = [];
            foreach( $headers as $i => $h )
            {
                $batch[$h] = $row[$i];
            }
            $batchs[] = $batch ;
        }
        fclose( $csv );
        return $batchs ;
    }
}
