<?php

namespace App\Console\Commands;

use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class HttpCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dhw:http'
        .' {action : Action to run}'
        .' {--url= : an url if action need it}'
        .' {--xpath= : an xpath query if action need it}'
        ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Some http stuff'
        ."\n  ".'- actions:'
        ."\n    ".'- "tryDom" needs "--url" and "--xpath"'
        ;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $action = $this->argument('action') ;
        $this->info( $action );
        switch( $action )
        {
            case 'tryDom':
                $this->tryDom();
                break;
        }

        return 0;
    }

    /**
     * examples:
     * - ./artisan dhw:http tryDom --url=https://code.devhost --xpath="//*"
     *
     * @return void
     */
    protected function tryDom()
    {
        $internalErrors = libxml_use_internal_errors(true);

        $dom = new DOMDocument();
        $html = '' ;

        $url = $this->option('url');
        $this->comment('Get '.$url);

        $response = Http
            ::withOptions(['verify' => false])
            ->get( $url);
        $this->comment('status: '.$response->status());

        $html = $response->body();
        $this->comment( 'Loaded '.strlen($html).' bytes');
        $html = mb_convert_encoding( $html,'HTML-ENTITIES','UTF-8' );

        file_put_contents('toto.html',$html );

        $dom->recover = true;
        $dom->strictErrorChecking = false;
        $dom->loadHTML(
            $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );
        //$this->warn( var_export(libxml_get_errors(),true ));
        $this->comment('Libxml errors count: '.count(libxml_get_errors()) );

        $xpath = new DOMXPath($dom);

        /*
         * examples:
         * 
         * query
         * //*[@name="csrf-token"]
         * on
         * <meta name="csrf-token" content="uVz2s8t2ueUk8qr3OcQgd74SHWyjXGbhTVLVTHc1" />
         * give:
         * nodeName: meta
         * attr: name = csrf-token
         * attr: content = uVz2s8t2ueUk8qr3OcQgd74SHWyjXGbhTVLVTHc1
         * 
        //
        // Result will be a "DOMElement"
        //$elements = $xpath->query('//*[@name="csrf-token"]');
        //
        // Result will be a "DOMAttr"
        //$elements = $xpath->query('//*[@name="csrf-token"]/@content');
        */

        $elements = $xpath->query( $this->option('xpath') );

        if( ! $elements )
        {
            $this->error('Failed XPath!');
            return -1 ;
        }
        $this->comment('Result "'.get_class($elements).'"');

        foreach ($elements as $element)
        {
            $this->info('element: nodeName:"'.$element->nodeName.'", class: "'.get_class($element).'"');
            switch( get_class($element) )
            {
                case DOMAttr::class:
                    $this->info('nodeValue: "'.$element->value.'"');
                    break;

                case DOMElement::class:
                    $nodes = $element->childNodes;
                    $this->comment('nodes '.count($nodes));
                    //if( is_array($nodes))
                    foreach ($nodes as $node) {
                        if( isset($node->name) )
                            $this->info('node: "'.$node->name.'" = "'.$node->nodeValue.'"');
                        else
                            $this->info('node: "'.$node->nodeValue.'"');
                    }
                    $attrs = $element->attributes;
                    $this->comment('attrs '.count($attrs));
                    //if( is_array($attrs) )
                    foreach ($attrs as $attr) {
                        $this->info('attr: "'.$attr->name.'" = "'.$attr->value.'"');
                    }
                    break;

                default:
                    $this->warn('Unknow type "'.get_class($element).'"');
            }
        }

    }
}
