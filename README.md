# Distributed Http Workers

Dispatch jobs around worker agents using http communication.

Répartie l'exécution de tâches sur d'autres serveurs web.

[TOC]

## About

Thanks to the great & lovely free software ecosystem !

- [Php](https://php.net)
- [Laravel](https://laravel.com)
- [JQuery](https://jquery.com)
- [Bootstrap](https://getbootstrap.com)
- [Jsoneditor](http://jsoneditoronline.org/)
- [Framasoft](https://framasoft.org/fr/), [April](april.org/), ...
- and so more !

## License

- FR: [Licence publique générale GNU](https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)
- EN: [GNU General Public License](https://en.wikipedia.org/wiki/GNU_General_Public_License)

## Runner

```text
Master -> Agent -> Runner
                -> Runner
       -> Agent -> Runner
                -> Runner
                -> Runner

Master ->AsyncHttp-> Agent ->AsyncHttp-> Runner
```

Extension `php-curl` is important to provides asynchronous http request with Guzzle.

### trade “Addition”

This trade computes inside the Runner.

`Master ->AsyncHttp-> Agent ->AsyncHttp-> Runner`

### trade “WebsiteScenario”

Runner for this trade does http calls, it is synchonous because scenario’s steps must be executed in order (*and it’s easier to code*).

`Master ->AsyncHttp-> Agent ->AsyncHttp-> Runner ->SyncHttp-> Scenario step's target`

### Error management

Technical error

- at agent level
- at runner level

Functionnal errror

- at runner level
- the Trade have to set error in WorkResult

## Trade "Addition"

Additionne 2 chiffres.

### examples

[doc/example_data/AgentWorkload-Addition_ok.json](doc/example_data/AgentWorkload-Addition_ok.json)

Donne comme résultat:

```php
# ./artisan dhw:dhw ./doc/example_data/AgentWorkload-Addition_ok.json count --count=1 --agent="https://dhw.devhost/api/dhw/agent|1"
array (
  'class' => 'App\\Trades\\Addition\\WorkResult',
  'batch' => 
  array (
    'a' => '1',
    'b' => '1',
  ),
  'result' => 
  array (
    'sum' => 2,
  ),
  'ellapsed_ms' => 1.1920928955078125E-5,
)
```

[doc/example_data/AgentWorkload-Addition_error.json](doc/example_data/AgentWorkload-Addition_error.json)

Donne comme résultat:

```php
# ./artisan dhw:dhw ./doc/example_data/AgentWorkload-Addition_error.json batchs --agent="https://dhw.devhost/api/dhw/agent|1"
array (
  'class' => 'App\\Trades\\WorkResultError',
  'batch' => 
  array (
    0 => 
    array (
      'a' => '1',
      'b' => 'abc',
    ),
  ),
  'result' => 
  array (
    'exception' => 'InvalidArgumentException',
    'message' => '"a" & "b" must be numeric values',
    'origin' => 'ubuntu-NS50MU/dhw.devhost//fpm-fcgi//api/dhw/runner',
    'ellapsed_ms' => -1,
  ),
  'ellapsed_ms' => -1,
)
```

## Trade "WebsiteScenario"

Exécute un scénario constitué de requêtes web paramétrées avec gestion et extraction de variables.
Pour faire,
- des tests de charges
- des scappers

### Variables

- Autant que possible remplace la syntaxe "{{variable_name}}" par une valeur définie ou fournie dynamiquement au Scenario. Si "variable_name" n'est pas définie retourne tel quel "{{variable_name}}".

### Extraction XPath

- [DOMXPath](https://www.php.net/manual/en/class.domxpath.php)
- Affecte la valeur extraite à une variable

Quelques queries:
- `//div[@class=\"download\"]/ul/li[1]/a[@class=\"download-link\"]/@href`

### Extraction Regular Expression

- [Php PCRE](https://www.php.net/manual/fr/pcre.pattern.php)
- Affecte la valeur extraite à une variable

Quelques queries:
- `<li id='wp-admin-bar-site-name' class=\"menupop\">.* href='https:\\/\\/pdpat.devhost\\/'>(.*)<\\/a>`
- `<li id='wp-admin-bar-site-name' class=\"menupop\">.* href='{{home}}'>(.*)<\\/a>`

Voir l'exemple [WebsiteScenario02](#WebsiteScenario02) qui utilise un extracteur RegEx.

**Attention**: special regex escaping for "query" & "variables" :

- the variable content must be escaped for regex's delimiter "/"
- here variable "home" must be "https:\\/\\/pdpat.devhost\\/"
  - `<li id='wp-admin-bar-site-name' class=\"menupop\">.* href='{{home}}'>(.*)<\\/a>`

### examples

#### WebsiteScenario01

Avec la commande `dhw:dhw` :

```bash
./artisan dhw:dhw \
  ./doc/example_data/AgentWorkload-WebsiteScenario01.json \
  batchs \
  --agent="https://dhw.devhost/api/dhw/agent|4"
```

Avec la commande `dhw:webScenario` :

Le scénario [/doc/example_data/WebsiteScenario01.json](doc/example_data/WebsiteScenario01.json) récupère le lien pour télécharger la dernière version de Php. Il se rend sur la home de php.net, suit le 1er lien de la section "Download", et extrait le lien du 1er fichier à télécharger.

Ce lien est est disponible à la clé `variables.download_link` du dernier résultat:
```php
$ ./artisan dhw:webScenario ./doc/example_data/WebsiteScenario01.json 
array (
  0 => 
  App\Trades\WebsiteScenario\Results\ResultHttp::__set_state(array(
     'req_duration' => 0.36911916732788086,
     'resp_status' => 200,
     'resp_body_size' => 41935,
     'step_start_at' => 1621772539.519948,
     'step_end_at' => 1621772539.893833,
     'step_ellapsed' => 0.373884916305542,
     'variables' => 
    array (
      'url' => 'https://www.php.net/',
      'link1' => '/downloads.php#v8.0.6',
    ),
  )),
  1 => 
  App\Trades\WebsiteScenario\Results\ResultHttp::__set_state(array(
     'req_duration' => 0.382735013961792,
     'resp_status' => 200,
     'resp_body_size' => 17899,
     'step_start_at' => 1621772539.893838,
     'step_end_at' => 1621772540.278927,
     'step_ellapsed' => 0.3850891590118408,
     'variables' => 
    array (
      'url' => 'https://www.php.net/',
      'link1' => '/downloads.php#v8.0.6',
      'download_link' => '/distributions/php-8.0.6.tar.gz',
    ),
  )),
)
Scenario ellapsed secondes: 0.837
```

#### WebsiteScenario02

**TODO**: ajouter la Step `JobDone` et toutes les nouveautés

```bash
# Version pour la commande "dhw:dhw"
./artisan dhw:dhw \
  ./doc/example_data/AgentWorkload-WebsiteScenario02.json \
  batchs \
  --agent="https://dhw.devhost/api/dhw/agent|4" \
  --var="password_secret=the real good password" --var="username_secret=the username" \
  --var="site_name=Printemps de Poètes à Tours"
```



Avec la commande `dhw:webScenario` :

Le scénario [/doc/example_data/WebsiteScenario02.json](doc/example_data/WebsiteScenario02.json) s'identifie sur un Wordpress. Récupère la page de login et extrait les noms des paramètres dans les variables `user_name` et `user_pass` pour faire le Http:Post, puis, si l'indentification réussie, récupère le nom du site dans la variable `site_name`.

```bash
./artisan dhw:webScenario ./doc/example_data/WebsiteScenario02.json \
   --var="url=https://pdpat.devhost/wp-admin" \
   --var="username=SuperBoby" \
   --var="password=@@@PeaceLove&Fun@@@" \
   --var="home=https:\\/\\/pdpat.devhost\\/" \
   --var="ms_min=50" --var="ms_max=60"
```

La variable `home` est escapée pour la RegEx avec délimiteur `/`.

```php
Scenario results: array (
  0 => 
  App\Trades\WebsiteScenario\Results\ResultHttp::__set_state(array(
      'http_uri' => '...',
      'http_method' => 'GET',
     'req_duration' => 0.40925002098083496,
     'resp_status' => 200,
     'resp_body_size' => 8338,
     'step_start_at' => 1622644619.282228,
     'step_end_at' => 1622644619.692942,
     'step_ellapsed' => 0.41071391105651855,
     'variables' => 
    array (
      'url' => 'https://pdpat.devhost/wp-admin',
      'home' => 'https:\\/\\/pdpat.devhost\\/',
      'username' => 'SuperMan',
      'password' => 'loveislove',
      'ms_min' => '50',
      'ms_max' => '60',
      'login_url' => 'https://pdpat.devhost/wp-login.php',
      'user_login' => 'log',
      'user_pass' => 'pwd',
    ),
  )),
  1 => 
  App\Trades\WebsiteScenario\Results\ResultSleep::__set_state(array(
     'sleep_ms' => 59,
     'step_start_at' => 1622644619.692943,
     'step_end_at' => 1622644619.752331,
     'step_ellapsed' => 0.059387922286987305,
     'variables' => 
    array (
      'url' => 'https://pdpat.devhost/wp-admin',
      'home' => 'https:\\/\\/pdpat.devhost\\/',
      'username' => 'SuperMan',
      'password' => 'loveislove',
      'ms_min' => '50',
      'ms_max' => '60',
      'login_url' => 'https://pdpat.devhost/wp-login.php',
      'user_login' => 'log',
      'user_pass' => 'pwd',
    ),
  )),
  2 => 
  App\Trades\WebsiteScenario\Results\ResultHttp::__set_state(array(
      'http_uri' => '...',
      'http_method' => 'POST',
     'req_duration' => 1.3565349578857422,
     'resp_status' => 200,
     'resp_body_size' => 79952,
     'step_start_at' => 1622644619.752335,
     'step_end_at' => 1622644621.11084,
     'step_ellapsed' => 1.3585050106048584,
     'variables' => 
    array (
      'url' => 'https://pdpat.devhost/wp-admin',
      'home' => 'https:\\/\\/pdpat.devhost\\/',
      'username' => 'SuperMan',
      'password' => 'loveislove',
      'ms_min' => '50',
      'ms_max' => '60',
      'login_url' => 'https://pdpat.devhost/wp-login.php',
      'user_login' => 'log',
      'user_pass' => 'pwd',
      'site_name' => 'Printemps de Poètes à Tours',
    ),
  )),
)
Scenario ellapsed seconds: 1.829
```

Et pour faire un test de charge à partir de ce scénario, il y a le fichier [doc/example_data/AgentWorkload-WebsiteScenario02.json](doc/example_data/AgentWorkload-WebsiteScenario02.json) dans lequel il faut remplacer les `variables` définies dans les `batchs`.

La commande suivante va...

```bash
./artisan dhw:dhw \
 ./doc/example_data/AgentWorkload-WebsiteScenario02.json \
 count --count=50 \
 --agent="https://dhw.devhost/api/dhw/agent|4"
```



## Smoke of thoughts (*or Thoughts in smoke*)

- Faire exécuter des tâches à d'autres serveurs web
- Ces tâches sont typées, elles correspondent à un "métier" ([Trades](app/Trades)).
- Limitations
  - Timeout des serveurs (Http, Php)
  - Network out availability
  - Installed Php extensions
- Déploiement
  - Laravel est utilisé mais un peu lourd pour le déploiement des agents et sans moulte besoins de technologie.
    - vendor size (`du -hs vendor`):
      - with dev: 57Mo
      - without dev: 31Mo
    - Tout le dossier du projet: 5 305 élément, d’une taille de 24,4 Mo (36,8 Mo sur le disque)
    - Master Front use massively Javascript
      - Will it use database and need orm ? Don’t know yet...
    - Agents manage Json without GUI
    - Lumen `vendor` folder:
      - without dev: 3 831 élément, d’une taille de 14,3 Mo (23,4 Mo sur le disque)
- Les classes des objets échangés ont leurs attributs “public” pour ne pas avoir à coder leur serialization. Amélioration probable un autre jour :-)

