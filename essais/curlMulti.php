#!/usr/bin/env php
<?php
/**
 * Async requests with Curl
 */
require(__DIR__.'/../vendor/autoload.php');

$requestsMax = 15 ;

$baseUri = 'http://localhost:8000/guzzleAsync01-server.php' ;
$baseUri = 'https://dhw.devhost/essais/essai-async-server.php';

$curlMulti = curl_multi_init();

$clients = [];
for( $c=0; $c<$requestsMax; $c++ )
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $baseUri.'?var='.$c);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
    curl_multi_add_handle($curlMulti,$curl);
    $clients[] = $curl ;
}

do {
    curl_multi_exec($curlMulti, $running);
    curl_multi_select($curlMulti);
} while ($running > 0);

foreach( $clients as $cli => $curl ){
    echo curl_getinfo($curl, CURLINFO_HTTP_CODE);
    echo curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    echo "\n";

    curl_multi_remove_handle($curlMulti, $curl);
  }
  
  curl_multi_close($curlMulti);
