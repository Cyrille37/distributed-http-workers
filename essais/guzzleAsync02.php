#!/usr/bin/env php
<?php
/**
 * Async requests with Guzzle.
 * Essai #2
 * 
 * - 1 seul GuzzleHttp\Client
 * - autant de CookieJar que de "clients virtuels"
 * - le CookieJar est affecté au moment du sendAsync()
 * - MAIS les cookies reçus ne sont pas renvoyés au prochain send()...
 * 
 * De l'avis que les objets requetes sont clonés et les CookieJar aussi, du coup ils ne s'alimentent pas.
 * Voir guzzleAsync03.php qui lui a les cookies qui s'alimentent.
 */

require(__DIR__.'/../vendor/autoload.php');

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Cookie\SessionCookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Promise;
use GuzzleHttp\Handler\CurlMultiHandler;
use GuzzleHttp\HandlerStack;

$clientsMax = 2 ;
$requestsMax = $clientsMax * 5 ;

// Don't try with Php builtin server (php -S <host>:<port>) !
//$baseUri = 'http://localhost:8000/guzzleAsync01-server.php' ;
$baseUri = 'https://dhw.devhost/essais/essai-async-server.php';

$handler = new CurlHandler();
$stack = HandlerStack::create($handler); // Wrap w/ middleware

$http = new Client( [
    'verify'=>false,
    //'handler' => $stack,
    //'cookies' => true,
]);

$clients = [];
//session_start();
for( $c=0; $c<$clientsMax; $c++ )
{
    /*
    $cjar = new SessionCookieJar('C'.$c);
    $cjar->setCookie(new SetCookie([
        'Name'    => 'biscuit',
        'Value'   => $c,
        'Domain'  => 'dhw.devhost',
        'Expires' => \time() + 8600
        ]));
    $clients[] = [
        'cookies' => $cjar,
    ];
    */
    $clients[] = [
        'cookies' => CookieJar::fromArray([
            'biscuit'=>$c
        ], 'dhw.devhost'),
    ];
}

$promises = [];
for( $r=0; $r<$requestsMax;  )
{
    for( $c=0; $c<$clientsMax; $c++ )
    {
        $promises[] = $http->getAsync(
            $baseUri. '?var='.$c.'-'.$r,
            [
                'cookies' => $clients[$c]['cookies'],
            ]
        );
        $r ++ ;
    }
}

echo 'Waiting for requests...',"\n";

// Wait for all requests to complete, even if some of them fail
$responses = Promise\Utils::settle($promises)->wait();

echo 'Jobs done.',"\n";

foreach( $responses as $k => $r )
{
    echo "\t", $k, ' ', $r['state'],"\n";
}

echo 'Cookies:', "\n";
foreach( $clients as $c )
{
    echo "\t", var_export($c['cookies']->toArray(),true) ,"\n";
}
