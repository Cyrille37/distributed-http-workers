#!/usr/bin/env php
<?php
/**
 * Async requests with Guzzle.
 * Essai #3
 * 
 * - use of $promise->then( callback_ok(), callback_failed) form
 * - to reinject cookieJar in request
 */

require(__DIR__.'/../vendor/autoload.php');

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Cookie\SessionCookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Promise;
use GuzzleHttp\Handler\CurlMultiHandler;
use GuzzleHttp\HandlerStack;
use Psr\Http\Message\ResponseInterface;

$clientsMax = 2 ;
$requestsMax = 5 ;

// Don't try with Php builtin server (php -S <host>:<port>) !
//$baseUri = 'http://localhost:8000/guzzleAsync01-server.php' ;
$baseUri = 'https://dhw.devhost/essais/essai-async-server.php';

$http = new Client( [
    'verify'=>false,
]);

$promises = [];
$clients = [];
for( $c=0; $c<$clientsMax; $c++ )
{
    $clients[] = [
        'cookies' => CookieJar::fromArray([
            'biscuit'=>$c
        ], 'dhw.devhost'),
        'steps'=>1,
        'cb_ok' => function(ResponseInterface $res) use ($c, &$promises, $http, $baseUri, &$clients, $requestsMax)
        {
            echo 'Headers C',$c, ' : ', var_export( $res->getHeaders(),true ),"\n";
            if( $clients[$c]['steps'] > $requestsMax)
                return ;
            $promise = $http->getAsync(
                $baseUri. '?var='.$c.'-'.(++$clients[$c]['steps']),
                [
                    'cookies' => $clients[$c]['cookies'],
                ]
            );
            $promise->then( $clients[$c]['cb_ok'], $clients[$c]['cb_fail']);
            $promises[] = $promise ;
        },
        'cb_fail' => function(RequestException $e) use ($c, $promises)
        {

        },
    ];
}

for( $c=0; $c<$clientsMax; $c++ )
{
    $promise = $http->getAsync(
        $baseUri. '?var='.$c.'-'.$r,
        [
            'cookies' => $clients[$c]['cookies'],
        ]
    );
    $promise->then( $clients[$c]['cb_ok'], $clients[$c]['cb_fail']);
    $promises[] = $promise ;
}

echo 'Waiting for requests...',"\n";

// Wait for all requests to complete, even if some of them fail
$responses = Promise\Utils::settle($promises)->wait();

echo 'Jobs done.',"\n";

foreach( $responses as $k => $r )
{
    echo "\t", $k, ' ', $r['state'],"\n";
}

echo 'Cookies:', "\n";
foreach( $clients as $c )
{
    echo "\t", var_export($c['cookies']->toArray(),true) ,"\n";
}
