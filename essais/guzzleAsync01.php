#!/usr/bin/env php
<?php
/**
 * Async requests with Guzzle.
 * Essai #1.
 * 
 * Les requetes sont asynchrones pour 1 client.
 * Quand plusieurs clients,
 * - toutes les requetes d'un client sont d'abord effectuées, en async,
 * - puis client suivant...
 */

require(__DIR__.'/../vendor/autoload.php');

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Promise;
use GuzzleHttp\Handler\CurlMultiHandler;
use GuzzleHttp\HandlerStack;

$clientsMax = 3 ;
$requestsMax = $clientsMax * 10 ;

// Don't try with Php builtin server (php -S <host>:<port>) !
//$baseUri = 'http://localhost:8000/guzzleAsync01-server.php' ;
$baseUri = 'https://dhw.devhost/essais/essai-async-server.php';

$clients = [];
for( $c=0; $c<$clientsMax; $c++ )
{
    $clients[] = new Client( [
        'base_uri' => $baseUri,
        'verify'=>false,
        //'handler' => $handler,
    ] );
}

$promises = [];
for( $r=0; $r<$requestsMax;  )
{
    for( $c=0; $c<$clientsMax; $c++ )
    {
        $promises[] = $clients[$c]->getAsync('?var='.$c.'-'.$r );
        $r ++ ;
    }
}

echo 'Waiting for requests...',"\n";

// Wait for all requests to complete, even if some of them fail
$responses = Promise\Utils::settle($promises)->wait();

echo 'Jobs done.',"\n";

foreach( $responses as $k => $r )
{
    echo "\t", $k, ' ', $r['state'],"\n";
}