<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('dhw')->name('dhw.')
    ->namespace('\App\Http\Controllers\Dhw')
    ->group( function ()
{
    Route::get('/','MasterController@index')->name('index');
});
