/**
 * Dhw.js
 */
"use strict";

DHW.Events.EVT_AGENT_START = 'dhw.agent.start';
DHW.Events.EVT_AGENT_STOP = 'dhw.agent.stop';

DHW.Agent = function( uuid, url )
{
    var self = this ;
    this.uuid = uuid ;
    this.url = url ;

    this.start = function()
    {
        console.log('Agent starting...', self.uuid );

        var ev = new CustomEvent(DHW.Events.EVT_AGENT_START, {
            detail: { uuid: self.uuid }
          });
        window.dispatchEvent( ev );
    };

    this.stop = function()
    {
        console.log('Agent stopping...', self.uuid );

        var ev = new CustomEvent(DHW.Events.EVT_AGENT_STOP, {
            detail: { uuid: self.uuid }
          });
        window.dispatchEvent( ev );
    };
}
