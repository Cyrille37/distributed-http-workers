/**
 * Dhw.js
 */
"use strict";

var DHW = {};
DHW.Events = {};

DHW.AgentsList = function( options )
{
    var config = {
        html_list: '#agents-list',
        html_tpl: '#tmpl-agent',
    };
    for( var o in options )
    {
        if( config[o] !== undefined )
        config[o] = options[o];
    }
    console.log('AgentsList config', config);

    var agents = {};
    var $list = $(config.html_list);
    var $tpl = $(config.html_tpl);

    window.addEventListener( DHW.Events.EVT_AGENT_START, function(ev)
    {
        console.log('Agent started.', ev.detail.uuid );
        $('.start', ('#'+ev.detail.uuid, $list) ).prop('disabled',true);
        $('.stop', ('#'+ev.detail.uuid, $list) ).prop('disabled',false);
    });

    window.addEventListener( DHW.Events.EVT_AGENT_STOP, function(ev)
    {
        console.log('Agent stopped.', ev.detail.uuid);
        $('.start', ('#'+ev.detail.uuid, $list) ).prop('disabled',false);
        $('.stop', ('#'+ev.detail.uuid, $list) ).prop('disabled',true);
    });

    this.createAgent = function( label, host )
    {
        if( ! host )
        {
            console.error('AgentsList.createAgent() invalid host "'+host+'"');
            return ;
        }
        var uuid = DHW.Uuid();

        var agent = new DHW.Agent( uuid, host );
        agents[uuid] = agent ;

        var $agent = $( $tpl.html() ).prop('id', uuid);
        $('.label',$agent).html( label );
        $('.host',$agent).html( host );
        $('.start',$agent).on('click', {uuid: uuid}, this.onStartAgent );
        $('.stop',$agent).on('click', {uuid: uuid}, this.onStopAgent );

        $list.append( $agent );
    };

    this.onStartAgent = function(ev)
    {
        console.log('Start Agent', ev.data.uuid );
        $(ev.target).prop('disabled',true);
        agents[ev.data.uuid].start();
    };

    this.onStopAgent = function(ev)
    {
        console.log('Stop Agent', ev.data.uuid );
        $(ev.target).prop('disabled',true);
        agents[ev.data.uuid].stop();
    };

};

/**
 * https://www.educba.com/javascript-uuid/
 * @returns string
 */
DHW.Uuid = function()
{
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace( /[xy]/g , function(c)
        {
            var rnd = Math.random()*16 |0, v = c === 'x' ? rnd : (rnd&0x3|0x8) ;
            return v.toString(16);
        });
}