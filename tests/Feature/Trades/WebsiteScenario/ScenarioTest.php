<?php

namespace Tests\Feature\Trades\WebsiteScenario ;

use App\Trades\WebsiteScenario\Scenario;

//use PHPUnit\Framework\TestCase;
use Tests\Feature\TestCase;
use Illuminate\Support\Facades\Http as FacadesHttp;
use \Illuminate\Http\Client\Request as ClientRequest ;

class ScenarioTest extends TestCase
{
    protected static function getScenario01()
    {
        $f = 'scenario01.json';
        return file_get_contents( __DIR__.'/'.$f);
    }

    /**
     * Scenario run
     *
     * @todo Something
     * @return void
     */

    public function testRun01()
    {
        $this->createApplication();
        FacadesHttp::fake(function (ClientRequest $request)
        {
            //echo 'FakeHttp: ',get_class($request),', url: ', print_r($request->url(),true),"\n";
            return FacadesHttp::response('Ok', 200);
        });

        $s = new Scenario( self::getScenario01() );
        $s->run();
        $this->assertTrue(true);
    }
}