<?php

namespace Tests\Feature\Trades\WebsiteScenario\Steps ;

use App\Trades\WebsiteScenario\Results\ResultHttp;
use App\Trades\WebsiteScenario\Scenario;
use App\Trades\WebsiteScenario\Steps\Get;
use App\Trades\WebsiteScenario\Steps\Http;
use App\Trades\WebsiteScenario\Steps\Step;
use App\Trades\WebsiteScenario\Variables;
use Mockery;
//use PHPUnit\Framework\TestCase;
use Tests\Feature\TestCase;
use Illuminate\Support\Facades\Http as FacadesHttp;
use \Illuminate\Http\Client\Request as ClientRequest ;
use Symfony\Component\HttpFoundation\Response ;

class GetTest extends TestCase
{
    public function testRunResultHttp()
    {
        $stepData = '{"class": "Get", "url": "{{url}}"}';

        // fake HTTP

        $this->createApplication();
        FacadesHttp::fake(function (ClientRequest $request)
        {
            //echo 'FakeHttp: ',get_class($request),', url: ', print_r($request->url(),true),"\n";
            return FacadesHttp::response('Ok', 200);
        });

        $step = Step::build( $stepData );
        /**
         * @var ResultHttp $result
         */
        $result = $step->run();

        $this->assertInstanceOf( ResultHttp::class, $result );
        $this->assertNotNull( $result->req_duration);
        $this->assertNotNull( $result->resp_body_size);
        $this->assertEquals( Response::HTTP_OK, $result->resp_status );
    }

    /**
     * Test variable replacement, mocking Variables::variableProcess'.
     * 
     * @return void
     */
    public function testRunVariableProcess()
    {
        $url = 'coucou';
        $stepData = '{'
                .'"class": "Get", "url": "{{url}}",'
                .'"params":[],"extracts":[]'
                .'}';

        // mock a "Variables" provider

        $mock = $this->createMock(Variables::class);
        $mock->expects($this->atLeast(1))
            ->method('variableProcess')
            ->withConsecutive(
                [$this->stringContains('url')],
                //[$this->stringContains('url')],
            )
            //->willReturn( $ret.'1', $ret.'2')
            ->willReturn( $url )
            ;

        // fake HTTP

        $this->createApplication();

        /*
        FacadesHttp::shouldReceive('withOptions')
            ->withAnyArgs()
            ;
        FacadesHttp::shouldReceive('get')
            ->once()
            ->with('key')
            ->andReturn('value');
        FacadesHttp::fake(
            [
                // Stub a JSON response for GitHub endpoints...
                'github.com/*' => FacadesHttp::response(['foo' => 'bar'], 200, $headers),
            
                // Stub a string response for Google endpoints...
                'google.com/*' => FacadesHttp::response('Hello World', 200, $headers),
            ]
        );*/

        FacadesHttp::fake(function (ClientRequest $request)
        {
            //echo 'FakeHttp: ',get_class($request),', url: ', print_r($request->url(),true),"\n";
            return FacadesHttp::response('Ok', 200);
        });

        $step = Step::build(
            $stepData, $mock
        );
        $step->run();

        /*Http::assertSent(function (Request $request) {
            return $request->hasHeader('X-First', 'foo') &&
                   $request->url() == 'http://example.com/users' &&
                   $request['name'] == 'Taylor' &&
                   $request['role'] == 'Developer';
        });*/

        FacadesHttp::assertSent(function (ClientRequest $request) use ($url)
        {
            //echo 'Expect:', $url, ' Req:',$request->url(),"\n";
            return $request->url() == $url ;
        });

    }

    /**
     * Test "variable" extraction, mocking Variables::variableSet'.
     * 
     * @return void
     */
    public function testRunVariableSet()
    {
        $variable_name = 'title';
        $variable_value = 'Framasoft';
        $stepData = [
            'class' => 'Get',
            'url' => 'test123',
            'extractors' => [
                [
                    "class" => "XPathDomAttrExtractor",
                    "query" => "//*[@name=\"twitter:title\"]/@content",
                    'variable' => '{{'.$variable_name.'}}',
                ]
            ],
        ];

        $content = '<!DOCTYPE html><html lang="fr"><head><meta charset="utf-8"><meta name="twitter:title" content="Framasoft"></head><body/></html>';

        // the "Variables" provider

        $variablesMock = $this->createMock(Variables::class);
        $variablesMock->expects($this->atLeast(1))
            ->method('variableProcess')
            //->willReturn( 'toto' )
            ->will($this->returnCallback(
                function($arg) use ($variable_name)
                {
                    //echo 'variableProcess.callback', ' arg:', $arg, "\n";
                    $retValue = null;
                    switch($arg)
                    {
                        // replace variable "{{title}}" by "title"
                        case '{{'.$variable_name.'}}';
                            $retValue = $variable_name ; 
                            break;
                        default:
                        $retValue = $arg;
                    }
                    return $retValue;    
                }
            ))
            ;
        $variablesMock->expects($this->atLeast(1))
            ->method('variableSet')
            ->withConsecutive(
                [$this->equalTo($variable_name), $this->equalTo($variable_value)],
        )
        ;
     
        // fake HTTP

        $this->createApplication();

        FacadesHttp::fake(function (ClientRequest $request) use($content)
        {
            return FacadesHttp::response($content, 200);
        });

        // build & run the Step.

        $step = Step::build( $stepData, $variablesMock );
        $step->run();

    }

    /**
     * Test Get with hardcoded parameters
     *
     * @return void
     */
    public function testRunParametersFixed()
    {
        $stepData = [
            'class' => 'Get',
            'url' => 'test123',
            'params' => [
                'Name' => 'John',
                'Age' => 123,
            ],
        ];

        $this->createApplication();

        FacadesHttp::fake(function (ClientRequest $request)
        {
            return FacadesHttp::response('Ok', 200);
        });

        // build & run the Step.

        $step = Step::build( $stepData );
        $step->run();

        FacadesHttp::assertSent(function (ClientRequest $request) use ($stepData)
        {
            //echo ' Req.Data:',print_r($request->data()),"\n";
            return isset($request['Name'])
                && $request['Name'] == $stepData['params']['Name']
                && isset($request['Age'])
                && $request['Age'] == $stepData['params']['Age'];
        });

    }

    /**
     * Test Get with variable parameters name & value
     *
     * @return void
     */
    public function testRunParametersVariable()
    {
        $variables = [
            ['name', 'John'],
            ['age', 'Age']
        ];
        $variable_name = 'name';
        $variable_value = 'John';
        $stepData = [
            'class' => 'Get',
            'url' => 'test123',
            'params' => [
                // Variable parameter value
                'Name' => '{{'.$variables[0][0].'}}',
                // Variable parameter name
                '{{'.$variables[1][0].'}}' => 123,
            ],
        ];

        $variablesMock = $this->createMock(Variables::class);
        $variablesMock->expects($this->atLeast(1))
            ->method('variableProcess')
            //->willReturn( 'toto' )
            ->will($this->returnCallback(
                function($arg) use ($variables)
                {
                    $retValue = null;
                    switch($arg)
                    {
                        // replace variable "{{name}}" by "John"
                        case '{{'.$variables[0][0].'}}';
                            $retValue = $variables[0][1] ; 
                            break;
                        // replace variable "{{age}}" by "Age"
                        case '{{'.$variables[1][0].'}}';
                            $retValue = $variables[1][1] ; 
                            break;
                        default:
                            $retValue = $arg;
                    }
                    return $retValue;    
                }
            ))
            ;

        $this->createApplication();

        FacadesHttp::fake(function (ClientRequest $request)
        {
            return FacadesHttp::response('Ok', 200);
        });

        // build & run the Step.

        $step = Step::build( $stepData, $variablesMock );
        $step->run();

        FacadesHttp::assertSent(function (ClientRequest $request) use ($stepData, $variables)
        {
            //echo ' Req.Data:',print_r($request->data()),"\n";
            return isset($request['Name'])
                && $request['Name'] == $variables[0][1]
                && isset($request[$variables[1][1]])
                && $request[$variables[1][1]] == 123
                ;
        });

    }

}
