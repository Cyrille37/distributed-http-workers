<?php

namespace Tests\Feature\Trades\WebsiteScenario\Steps ;

use App\Trades\WebsiteScenario\Steps\Post;
use App\Trades\WebsiteScenario\Steps\Step;
use App\Trades\WebsiteScenario\Variables;
use Tests\Feature\TestCase;
use Illuminate\Support\Facades\Http as FacadesHttp;
use Illuminate\Http\Client\Request as ClientRequest ;
use Symfony\Component\HttpFoundation\Request as HttpRequest ;

class PostTest extends TestCase
{
        /**
     * Test Get with hardcoded parameters
     *
     * @return void
     */
    public function testRunParametersFixed()
    {
        $stepData = [
            'class' => 'Post',
            'url' => 'test123',
            'params' => [
                'Name' => 'John',
                'Age' => 123,
            ],
        ];

        $this->createApplication();

        FacadesHttp::fake(function (ClientRequest $request)
        {
            return FacadesHttp::response('Ok', 200);
        });

        // build & run the Step.

        $step = Step::build( $stepData );

        $step->run();

        FacadesHttp::assertSent(function (ClientRequest $request) use ($stepData)
        {
            //echo 'Meth:', $request->method(), ', Data:',print_r($request->data()),"\n";
            return $request->method() == HttpRequest::METHOD_POST
                && isset($request['Name'])
                && $request['Name'] == $stepData['params']['Name']
                && isset($request['Age'])
                && $request['Age'] == $stepData['params']['Age'];
        });

    }

}