<?php

namespace Tests\Unit\Trades\WebsiteScenario ;

use App\Trades\WebsiteScenario\Scenario;

use Tests\Unit\TestCase;
use Illuminate\Support\Facades\Http as FacadesHttp;
use \Illuminate\Http\Client\Request as ClientRequest ;

class ScenarioTest extends TestCase
{
    protected static function getScenario01()
    {
        $f = 'scenario01.json';
        return file_get_contents( __DIR__.'/'.$f);
    }

    /**
     * Variables replacement.
     * Var exists -> replacement
     * Var does not -> no change
     * 
     * @return void
     */
    public function testVariable01()
    {
        $vars = [
            '{{abc}}' => '{{abc}}',
            '{{def}}' => '{{def}}',
            '{{g.h}}' => '{{g.h}}',
        ];
        $scn = new Scenario( self::getScenario01() );
        // 1. Variables doesn't exist
        foreach( $vars as $var => $expected )
        {
            $this->assertEquals( $expected, $scn->variableProcess( $var ) );
        }
        // 2. Some variables exists
        $vars['{{def}}'] = 'Hello!';
        $scn->variableSet('def', 'Hello!');
        $vars['{{g.h}}'] = 'Ciao!';
        $scn->variableSet('g.h', 'Ciao!');
        foreach( $vars as $var => $expected )
        {
            $this->assertEquals( $expected, $scn->variableProcess( $var ) );
        }
    }

    /**
     * Scenario construction.
     *
     * @return void
     */
    public function testNew01()
    {
        $scn =  new Scenario( self::getScenario01() );
        $this->assertTrue(true);
    }

}