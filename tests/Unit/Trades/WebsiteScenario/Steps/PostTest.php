<?php

namespace Tests\Unit\Trades\WebsiteScenario\Steps ;

use App\Trades\WebsiteScenario\Steps\Post;
use App\Trades\WebsiteScenario\Steps\Step;
use Tests\Unit\TestCase;

class PostTest extends TestCase
{
    /**
     * @return void
     */
    public function testBuildFromString()
    {
        $step = Step::build(
            '{"class": "Post", "url": "{{url}}"}'
        );
        $this->assertInstanceOf(Post::class, $step);
    }

    /**
     * @return void
     */
    public function testBuildFromArray()
    {
        $step = Step::build(
            json_decode('{"class": "Post", "url": "{{url}}"}', true )
        );
        $this->assertInstanceOf(Post::class, $step);
    }
}
