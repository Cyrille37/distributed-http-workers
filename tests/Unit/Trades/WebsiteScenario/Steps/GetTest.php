<?php

namespace Tests\Unit\Trades\WebsiteScenario\Steps ;

use App\Trades\WebsiteScenario\Steps\Get;
use App\Trades\WebsiteScenario\Steps\Step;
use Tests\Unit\TestCase;

class GetTest extends TestCase
{
    /**
     * @return void
     */
    public function testBuildFromString()
    {
        $step = Step::build(
            '{"class": "Get", "url": "{{url}}"}'
        );
        $this->assertInstanceOf(Get::class, $step);
    }

    /**
     * @return void
     */
    public function testBuildFromArray()
    {
        $step = Step::build(
            json_decode('{"class": "Get", "url": "{{url}}"}', true )
        );
        $this->assertInstanceOf(Get::class, $step);
    }

}
