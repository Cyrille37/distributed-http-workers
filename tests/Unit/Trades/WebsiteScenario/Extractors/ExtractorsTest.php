<?php

namespace Tests\Unit\Trades\WebsiteScenario\Extractors ;

use App\Trades\WebsiteScenario\Extractors\Extractor;
use App\Trades\WebsiteScenario\Extractors\XPathDomAttrExtractor;
use App\Trades\WebsiteScenario\Variables;
use Tests\Unit\TestCase;

class ExtractorsTest extends TestCase
{
    public function testXPathDomAttrExtractor()
    {
        $variable_name = 'title' ;
        $variable_value = 'Framasoft' ;

        $content = '<!DOCTYPE html><html lang="fr"><head><meta charset="utf-8">'
            .'<meta name="twitter:title" content="Framasoft">'
            .'</head><body/></html>';

        $extractorData = [
            "class" => "XPathDomAttrExtractor",
            "query" => "//*[@name=\"twitter:title\"]/@content",
            'variable' => $variable_name
        ];

        // mock the "Variables" provider

        $variablesMock = $this->createMock(Variables::class);

        $variablesMock->expects($this->atLeast(1))
            ->method('variableProcess')
            ->will($this->returnCallback(
                function($arg)
                {
                    //echo 'variableProcess.callback', ' arg:', $arg, "\n";
                    $retValue = $arg;
                    return $retValue;    
                }
            ))
            ;

        $variablesMock->expects($this->atLeast(1))
            ->method('variableSet')
            ->withConsecutive(
                [$this->stringContains($variable_name)],
                //[$this->stringContains('url')],
            )
            //->willReturn( $ret.'1', $ret.'2')
            //->willReturn( $variable_value )
            ->will($this->returnCallback(
                function($arg)
                {
                    return $arg;    
                }))
            ;

        $extractor = Extractor::build( $extractorData, $variablesMock );
        $this->assertInstanceOf( XPathDomAttrExtractor::class, $extractor );

        $value = $extractor->extract( $content );
        $this->assertEquals($variable_value, $value );
    }
}
