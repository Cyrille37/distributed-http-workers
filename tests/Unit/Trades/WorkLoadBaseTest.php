<?php

namespace Tests\Unit\Trades ;

use App\Trades\Addition\WorkLoad as AdditionWorkLoad;
use App\Trades\WorkLoadBase;
use Tests\Unit\TestCase;

class WorkLoadBaseTest extends TestCase
{
    public function testAccessors()
    {
        $wlAr = [
            'batchs'=>[ [1,2,3] ],
            'trade' => [ 'class'=>'Toto', 'data'=>[4,5,6] ]
        ];
        $dummyWorkLoad = new class( $wlAr ) extends WorkLoadBase
        {
            public function __construct( array $workLoadArray )
            {
                parent::__construct( $workLoadArray );
            }
        };
        
        $this->assertNotEmpty( $dummyWorkLoad );
        $this->assertEquals( $wlAr['batchs'], $dummyWorkLoad->getBatchs() );
        $this->assertEquals( $wlAr['trade']['class'], $dummyWorkLoad->getTradeClass() );
        $this->assertEquals( $wlAr['trade']['data'], $dummyWorkLoad->getTrade()['data'] );
    }

    public function testBuildAdditionWorkLoad()
    {
        $wlAr = $this->getExampleAgentWorkloadAddition( $asArray = true );

        $additionWL = WorkLoadBase::build( $wlAr );

        $this->assertNotEmpty( $additionWL );
        $this->assertInstanceOf( AdditionWorkLoad::class, $additionWL );
    }
}
