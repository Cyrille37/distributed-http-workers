<?php

namespace Tests\Unit\Trades ;

use App\Trades\Trades;
use App\Trades\WorkResultBase;
use App\Trades\WorkResultError;
use Illuminate\Support\Facades\App;
use Tests\Unit\TestCase;

class WorkResultTest extends TestCase
{
    public function testAccessors()
    {
        $data = [
            'batch'=>[ [1,2,3] ],
            'result' => [ 'class'=>'Toto', 'data'=>[4,5,6] ],
            'ellapsed' => 0.123
        ];

        $workResult = new class(
                $data['batch'],
                $data['result'],
                $data['ellapsed']
            ) extends WorkResultBase
            {
            };

        $this->assertNotEmpty( $workResult );
        $this->assertEquals( get_class($workResult), $workResult->getClass() );
        $this->assertEquals( $data['batch'], $workResult->getBatch() );
        $this->assertEquals( $data['result'], $workResult->getResult() );
        $this->assertEquals( $data['ellapsed'], $workResult->getEllapsedMs() );
    }

    public function testErrorFromException()
    {
        $batch = [1,2,3] ;
        $exception = new \InvalidArgumentException('La valeur n’est pas celle attendue !');

        // PhpUnit removed the static method mocking :(
        /*$variablesMock = $this->createMock(Trades::class);
        $variablesMock->staticExpects( $this->atLeast(1) )
            ->method('whereAmI')
            ->will($this->returnValue('ici et là')
            );*/
        // So mocking it's dependencies (Thanks to Laravel Facade easy Mocking!).
        App::shouldReceive('runningInConsole')
            ->twice()
            //->with('key')
            ->andReturn(true);

        $wre = new WorkResultError( $batch, $exception );
        $this->assertEquals( $batch, $wre->getBatch() );
        $this->assertEquals( WorkResultBase::INVALID_ELLAPSED, $wre->getEllapsedMs() );
        $this->assertEquals( \InvalidArgumentException::class, $wre->getExceptionClass() );
        $this->assertEquals( $exception->getMessage(), $wre->getMessage() );
        $this->assertEquals( Trades::whereAmI(), $wre->getOrigin() );
    }

    public function testBuildWorkResultError()
    {
        $data = [
            //'class' => 'App\Trades\WorkResultError',
            'class' => WorkResultError::class,
            'batch'=>[ [1,2,3] ],
            'result' => [
                'exception'=>'InvalidArgumentException',
                'message'=>'Oups, an error occured because of this or that',
                'origin' => 'Ici et là'
            ],
            'ellapsed_ms' => WorkResultBase::INVALID_ELLAPSED
        ];

        /**
         * @var WorkResultError $wre 
         */
        $wre = WorkResultBase::build( $data );

        $this->assertInstanceOf( WorkResultError::class, $wre );
        $this->assertEquals( $data['batch'], $wre->getBatch() );
        $this->assertEquals( $data['ellapsed_ms'], $wre->getEllapsedMs() );

        $this->assertEquals( $data['result']['exception'], $wre->getExceptionClass() );
        $this->assertEquals( $data['result']['message'], $wre->getMessage() );
        $this->assertEquals( $data['result']['origin'], $wre->getOrigin() );
    }

}