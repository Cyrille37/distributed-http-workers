<?php

namespace Tests\Unit ;

use PHPUnit\Framework\TestCase as TestCaseBase ;
//use Tests\TestCase;

class TestCase extends TestCaseBase
{
    /**
     * Get "/doc/example_data" folder.
     *
     * @return string
     */
    public function getExampleDataFolder()
    {
        return __DIR__.'/../../doc/example_data' ;
    }

    /**
     * Load example file "AgentWorkload-Addition.json".
     *
     * @param boolean $asArray
     * @return Array|Object
     */
    public function getExampleAgentWorkloadAddition( $asArray )
    {
        return json_decode(
            file_get_contents(
                $this->getExampleDataFolder().'/AgentWorkload-Addition_ok.json'),
            $associative = $asArray
        );    
    }

}
