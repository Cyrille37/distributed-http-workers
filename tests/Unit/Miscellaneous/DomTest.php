<?php

namespace Tests\Unit\Miscellaneous ;

use App\Trades\WebsiteScenario\Scenario;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMXPath;
use PHPUnit\Framework\TestCase;

class DomTest extends TestCase
{
    protected function getDom01()
    {
        $f = 'html01.html';
        return file_get_contents( __DIR__.'/'.$f);
    }

    /**
     * Check some DOM query, not for testing for for documentation and exploration ;-)
     * @see \App\Console\Commands\HttpCmd
     *
     * @return void
     */
    public function testDOMElement01()
    {
        $dom = new DOMDocument();
        $html = $this->getDom01();

        $html = mb_convert_encoding( $html,'HTML-ENTITIES','UTF-8' );

        $internalErrors = libxml_use_internal_errors(true);
        $dom->recover = true;
        $dom->strictErrorChecking = false;
        $dom->loadHTML(
            $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );
        //$this->warn( var_export(libxml_get_errors(),true ));
        //$this->comment('Libxml errors count: '.count(libxml_get_errors()) );

        $xpath = new DOMXPath($dom);

        //
        // Result will be a "DOMElement"
        //<meta name="twitter:title" content="Framasoft">
        $elements = $xpath->query('//*[@name="twitter:title"]');

        $this->assertNotEmpty($elements);

        $element = $elements[0];
        $this->assertInstanceOf( DOMElement::class, $element );

        $nodes = $element->childNodes ;
        $this->assertEquals( 0, count($nodes));

        $attrs = $element->attributes ;
        $this->assertEquals( 2, count($attrs));
        $this->assertEquals( 'name', $attrs[0]->name);
        $this->assertEquals( 'twitter:title', $attrs[0]->value);
        $this->assertEquals( 'content', $attrs[1]->name);
        $this->assertEquals( 'Framasoft', $attrs[1]->value);
     }
 
    /**
     * Check some DOM query, not for testing for for documentation and exploration ;-)
     * @see \App\Console\Commands\HttpCmd
     *
     * @return void
     */
    public function testDOMAttr01()
    {
        $dom = new DOMDocument();
        $html = $this->getDom01();

        $html = mb_convert_encoding( $html,'HTML-ENTITIES','UTF-8' );

        $internalErrors = libxml_use_internal_errors(true);
        $dom->recover = true;
        $dom->strictErrorChecking = false;
        $dom->loadHTML(
            $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );
        //$this->warn( var_export(libxml_get_errors(),true ));
        //$this->comment('Libxml errors count: '.count(libxml_get_errors()) );

        $xpath = new DOMXPath($dom);

        // Result will be a "DOMAttr"
        //<meta name="twitter:title" content="Framasoft">
        $elements = $xpath->query('//*[@name="twitter:title"]/@content');
        $this->assertNotEmpty($elements);

        $element = $elements[0];
        $this->assertInstanceOf( DOMAttr::class, $element );
        $this->assertEquals('content', $element->name);
        $this->assertEquals('Framasoft', $element->value);
    }
 
}